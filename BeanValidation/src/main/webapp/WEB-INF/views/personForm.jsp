<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Person Form</title>
</head>
<body>

<c:forEach var="person" items="${map}" varStatus="status">
     <p>"${person.id} ${person.name} ${person.surname} ${person.salary}" </p>
</c:forEach>

<p></p>

<form:form method="post" id="addPersonForm" commandName="person"
			action="/bv/f/addPerson">
			<form:errors path="*" cssClass="errorblock" element="div"/>
			
			<p><label for="id">id:</label><form:input path="id" id="id" type="text" /></p>
			<p><label for="name">name:</label><form:input path="name" id="name" type="text" /></p>
			<p><label for="surname">surname:</label><form:input path="surname" id="surname" type="text" /></p>
			<p><label for="salary">salary:</label><form:input path="salary" id="salary" type="text" /></p>
			
			<p>
				<input id="SaveAccount" type="submit" class="saveButton" value="add Person" />
			</p>
</form:form>

</body>
</html>