package org.phoenix.model;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.Max;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.phoenix.validator.AcceptedValues;

public class Person2 implements Serializable {

	private static final long serialVersionUID = 14567946798436034L;

	@Min(value=0,message="Id is required")
	private int id;
	
	@NotEmpty(message="Name is required")
	@Length(min=2,max=32,message="Name should be between 2-32 charachters")
	private String name;
	
	@NotEmpty(message="Surname is required")
	private String surname;
	
	@Min(value=1000,message="Salary minimum value 1000")
	@Max(value=5000,message="Salary maxinum value 5000")
	private double salary;
	
	// offer can be: fixed,mobile,internet
	@AcceptedValues(acceptValues = { "fixed","mobile","internet" }, message = "Invalid offer")  
	private String offer;

	public Person2() {
	}

	
	public Person2(int id, String name, String surname, double salary, String offer) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.salary = salary;
		this.offer = offer;
	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}


	public String getOffer() {
		return offer;
	}


	public void setOffer(String offer) {
		this.offer = offer;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((offer == null) ? 0 : offer.hashCode());
		long temp;
		temp = Double.doubleToLongBits(salary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person2 other = (Person2) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (offer == null) {
			if (other.offer != null)
				return false;
		} else if (!offer.equals(other.offer))
			return false;
		if (Double.doubleToLongBits(salary) != Double.doubleToLongBits(other.salary))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Person2 [id=" + id + ", name=" + name + ", surname=" + surname + ", salary=" + salary + ", offer="
				+ offer + "]";
	}

	

}
