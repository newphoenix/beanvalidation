package org.phoenix.customValidator;

import org.phoenix.model.Person3;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@Qualifier("personValidator")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "prototype")
public class PersonValidator implements Validator {

	@Override
	public boolean supports(Class<?> c) {
		return Person3.class.isAssignableFrom(c);
	}

	@Override
	public void validate(Object target, Errors errors) {

		Person3 obj = (Person3) target;
		if (obj.getId() < 0) {
			errors.reject("id", "Id must be > 0");
		}

		if ("".equals(obj.getName().trim())) {
			errors.reject("name", "Name is required");
		}

		if ("".equals(obj.getSurname().trim())) {
			errors.reject("surname", "Surname is required");
		}
		if (obj.getSalary() < 1000 || obj.getSalary() > 5000) {
			errors.reject("salary", "Salary is between 1000 and 5000");
		}
	}

}
