package org.phoenix.com;

import java.util.HashMap;
import java.util.Map;

import org.phoenix.customValidator.PersonValidator;
import org.phoenix.model.Person3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/*
 * 
 * Bean validation
 * added offer property
 * validate offer property using custom validator AcceptedValues
 * 
 */

@Controller
@RequestMapping("/f3")
public class FormController3{
	
	@Autowired
	@Qualifier("personValidator")
	PersonValidator personValidator;
	
	public void setPersonValidator(PersonValidator personValidator) {
		this.personValidator = personValidator;
	}


	public static final Map<Integer,Person3> map3 = new HashMap<Integer,Person3>();
	{
		map3.put(1, new Person3(1,"John","Smith",1000.0));
		map3.put(2, new Person3(2,"Bill","Gates",2000.0));
		map3.put(3, new Person3(3,"Steve","Balmers",3000.0));
		map3.put(4, new Person3(4,"Larry","King",5000.0));
	}
	
	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public String getPersonForm(Model model) {
		
		
		model.addAttribute("map",map3.values());
		
		Person3 person = new Person3();		
		model.addAttribute("person3", person );
		
		return "personForm3";
	}
	
	
	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String addPersonForm(Person3 person3,BindingResult result,Model model) {	
		
		personValidator.validate(person3, result);
		 
		    if (result.hasErrors()) {
		        return "personForm3";
		    }		
			
		map3.put(person3.getId(), person3);
		model.addAttribute("map",map3.values());
	
		return "personList3";
	}

}
