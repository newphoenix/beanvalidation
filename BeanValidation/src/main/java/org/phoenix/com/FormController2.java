package org.phoenix.com;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
//import javax.validation.Validator;

import org.springframework.validation.Validator;

import org.phoenix.model.Person2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/*
 * 
 * Bean validation
 * added offer property
 * validate offer property using custom validator AcceptedValues
 * 
 */

@Controller
@RequestMapping("/f2")
public class FormController2{
	
	@Autowired	
	@Qualifier("validator")
	Validator validator;
	
	public static final Map<Integer,Person2> map2 = new HashMap<Integer,Person2>();
	{
		map2.put(1, new Person2(1,"John","Smith",1000.0,"Internet"));
		map2.put(2, new Person2(2,"Bill","Gates",2000.0,"Fixed"));
		map2.put(3, new Person2(3,"Steve","Balmers",3000.0,"Mobile"));
		map2.put(4, new Person2(4,"Larry","King",5000.0,"Internet"));
	}
	
	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public String getPersonForm(Model model) {
		
		
		model.addAttribute("map",map2.values());
		
		Person2 person = new Person2();		
		model.addAttribute("person2", person );
		
		return "personForm2";
	}
	
	
	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String addPersonForm(@Valid Person2 person2,BindingResult result,Model model) {	
		 
		    if (result.hasErrors()) {
		        return "personForm2";
		    }		
			
		map2.put(person2.getId(), person2);
		model.addAttribute("map",map2.values());
	
		return "personList2";
	}

}
