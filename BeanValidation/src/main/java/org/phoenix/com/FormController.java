package org.phoenix.com;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.Validator;


import org.phoenix.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/*
 * 
 * Bean validation
 * Minimal approach validation in Person bean only.
 * 
 */

@Controller
@RequestMapping("/f")
public class FormController{
	
	@Autowired
	Validator validator;
	
	public static final Map<Integer,Person> map = new HashMap<Integer,Person>();
	{
		map.put(1, new Person(1,"John","Smith",1000.0));
		map.put(2, new Person(2,"Bill","Gates",2000.0));
		map.put(3, new Person(3,"Steve","Balmers",3000.0));
		map.put(4, new Person(4,"Larry","King",5000.0));
	}
	
	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public String getPersonForm(Model model) {
		
		
		model.addAttribute("map",map.values());
		
		Person person = new Person();		
		model.addAttribute("person", person );
		
		return "personForm";
	}
	
	
	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String addPersonForm(@Valid Person person,BindingResult result,Model model) {	
		 
		    if (result.hasErrors()) {
		        return "personForm";
		    }		
			
		map.put(person.getId(), person);
		model.addAttribute("map",map.values());
	
		return "personList";
	}

}
